import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {WebViewer} from './src/components/common';
import ViewingPage from './src/components/ViewingPage';

export default class App extends React.Component {
  render() {
    return (
      <ViewingPage/>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
