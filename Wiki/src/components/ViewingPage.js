import React,{Component} from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {WebViewer,Input,Header} from './common';

class ViewingPage extends Component {
    state = {uri: ''};
  render() {
    return (
        <View style={styles.viewStyle}>
        <Header headerText={"Search Page"}/>
         <Input label={"Search keyword"} onChangeText={text => this.setState({uri: text})}/>
            <WebViewer source={{uri: this.state.uri}}/>  
        </View>
    );
  }
}
const styles = { 

    viewStyle :{
        flexDirection : 'column',
        flex : 1,
        justifyContent: 'space-between'
    },
    webArea : {

    }
};
export default ViewingPage;