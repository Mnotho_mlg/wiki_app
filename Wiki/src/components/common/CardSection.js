import React, {Component} from 'react';
import {Text, View} from 'react-native';

const CardSection = (props) =>
{
    return (

        <View style={[styles.containerStyle,props.style]} >
            {props.children}
        </View>
    );
};

const styles = {

    containerStyle : {

        borderBottomWidth : 0.5,
        padding : 5,
        backgroundColor : '#fff',
        justifyContent : 'flex-start',
        flexDirection : 'row',
        position : 'relative'

    }
};
export {CardSection};