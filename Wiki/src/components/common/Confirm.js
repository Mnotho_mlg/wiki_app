import React, {Component} from 'react';
import {Text, View, Modal} from 'react-native';
import {CardSection } from './CardSection';
import {Button } from './Button';

const Confirm = ({children,visible,onAccept,onDecline}) =>{
    //a modal is a wrapper that takes a fixed no. of componenets and renders them

    const {containerStyle, textStyle, cardSectionStyle }  = styles;

    return(

        <Modal
        animationType="slide" 
        onRequestClose={() => { console.log("You requested to close")}}
        transparent
        visible= {visible}>
            <CardSection style={cardSectionStyle}>
                <Text style={textStyle}>
                    {children}
                </Text>
            </CardSection>
            <CardSection>
                <Button onPress={onAccept} >Yes</Button>
                <Button onPress={onDecline}>No</Button>
            </CardSection>
        </Modal>
    );
};
const styles = {
    CardSectionStyle :{ 
        justifyContent : 'center'
    },
    textStyle : {
        flex : 1,
        textAlign : 'center',
        lineHeight : 40
    },
    containerStyle : {
        backgroundColor : 'rgba(0,0,0,0.75)',
        position : 'relative',
        flex:  1,
        justifyContent : 'center'
    }
};
export {Confirm};