import React from 'react';
import {Text, WebView, ActivityIndicator} from 'react-native';

const WebViewer = (props) =>
{
    console.log("props  "+ props.source.uri)
    return (
        <WebView source={{uri : props.source.uri}}
        javaScriptEnabled={true}
        domStorageEnabled={true}
        startInLoadingState={true}
        />

    );

}

const styles = {
    spinnerStyle:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    }
};
export {WebViewer};