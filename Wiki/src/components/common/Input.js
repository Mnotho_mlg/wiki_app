import React from 'react';
import {Text, View, TextInput,Dimensions} from 'react-native';

const {height, SCREEN_WIDTH} = Dimensions.get('window');
const Input = ({label,value, defaultValue, onChangeText,placeholder,secureTextEntry}) =>
{
    const {viewStyle,fieldStyle,textStyle,textInStyle,borderStyle} = styles;
    return(
        <View style={viewStyle} >
            <Text style={textStyle} >{label}</Text>
            <View style={borderStyle}>
                <TextInput style={textInStyle} onChangeText={onChangeText} defaultValue = {defaultValue} secureTextEntry={secureTextEntry}  placeholder={ placeholder} style={fieldStyle}></TextInput>
            </View>
        </View>
    );
};

const styles ={

    textInStyle :{
        borderRadius : 10,
    },
    viewStyle :{
        flexDirection : 'row',
        height : 40,
        marginTop : 10,
        display: 'flex',
        justifyContent: 'space-between'
    },
    fieldStyle :{
        height: 40, 
        color : '#000',
        paddingRight : 15,
        paddingLeft : 20,
        fontSize : 18,   
    },
    borderStyle : {
        borderColor:'grey',
        backgroundColor:'#D3D3D3',
        width : 220,
        borderWidth: 1,
        borderStyle: 'solid',
        fontSize:15,
        borderRadius: 10,
    },
    textStyle : {
        color : 'black',
        fontSize: 15,
        paddingLeft : 10,
        alignSelf : 'center'
       
     }
        
};
export {Input};