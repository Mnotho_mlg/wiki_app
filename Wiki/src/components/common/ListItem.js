import React,{Component} from 'react';
import {connect} from 'react-redux';
import {Scrollable} from 'react';
import {ListView, Text, 
TouchableWithoutFeedback, View,
LayoutAnimation} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {Card,CardSection} from './';


class ListItem extends Component{

    componentWillUpdate(){
        LayoutAnimation.spring();
    }

    //when user clicks on a name
    onRowPress()
    {
        //we need the values to be present when we open the form
        Actions.editEmployee({employee : this.props.result}); //but how do we pass the info to the form
        //the employee component will be given an additional prop of employee
    }
    render(){

       const {name} = this.props.employee;
       const {descriptionStyle, titleStyle} = styles;
        return( //on click, call the select library action, which gives commands to reducers
            <Card>
                <TouchableWithoutFeedback onPress={this.onRowPress.bind(this)}>
                    <View>
                        <CardSection >
                            <Text style={titleStyle}> 
                                {name}
                            </Text>
                        </CardSection>
                    </View>
                </TouchableWithoutFeedback>
                
            </Card>
        );
  
    }

}

const styles = {

    titleStyle: {
        fontSize: 18,
        paddingLeft: 15
    },
    descriptionStyle : {
        fontSize : 15,
        marginLeft : 7,
        marginRight : 7,
        flex: 1

    }
};


//connect => first arg : map state to props... second: actions
export {ListItem}; //actions will be passed down into the component as props