//file that houses our header component
//import Libs
import React, {Component} from 'react';
import {Text, ScrollView} from 'react-native';
import axios from 'axios';
import Album from './Album';

class AlbumList extends Component {

    state= {
        albums : []
    };
    componentWillMount()//method called everytime the component is to be rendered
    {
        console.log("Compone mount in albumlist")
        //returns a promise
        axios.get('https://rallycoding.herokuapp.com/api/music_albums') //http get request for data
        .then(response =>
                this.setState({albums : response.data})
        );//when data is returned
    }
   render(){
        //must be scrollable, so we use scroll view
        return(
            <ScrollView style={{backgroundColor : '#F8F8F8'}}>
                {this.renderAlbums()}
            </ScrollView>

        );
    }

    renderAlbums(){

       return this.state.albums.map(
            album => <Album key={album.title} album={album}/>
        ); 
    }
}

export default AlbumList;