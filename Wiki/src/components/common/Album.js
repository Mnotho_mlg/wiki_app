import React, {Component} from 'react';
import {Text, View, Image, Linking} from 'react-native';

import Card from './';
import CardSection from './'; //index.js eported them all
import Button from './';
//functional component cause it just shows something
const Album = (props) =>{

    const {CardSectionStyle,thumbnailStyle,
        thumbnailContainerStyle,headerStyle,
        headerTextStyle, imageStyle, artistTextStyle
            } = styles;
    const { title, artist, thumbnail_image, image, url } = props.album;
    return (
        <Card>
            <CardSection style={CardSectionStyle}>
                <View>
                    <Image 
                    style = {thumbnailStyle}
                        source={ {uri: thumbnail_image}} 
                    />
                </View>

                <View style={headerStyle}>
                    <Text style={headerTextStyle}>{title}</Text>
                    <Text style={artistTextStyle}>{artist}</Text>
                </View>
            </CardSection>

             <CardSection>
                 <Image 
                    style = {imageStyle}
                        source={ {uri: image}} 
                    />
            </CardSection>

            <CardSection>
                 <Button text={title} onPress={()=> Linking.openURL(url)}/>
            </CardSection>
        </Card>
    );
};

const styles = {

    CardSectionStyle :
    {
        flexDirection : 'row',
    },
    headerStyle : {
        flexDirection : 'column',
        justifyContent : 'space-around',
    },
    thumbnailStyle :
    {
        height : 60,
        width : 60
    },
    thumbnailContainerStyle:{
        justifyContent : 'center',
        alignContent : 'center',
        marginLeft : 10,
        marginRight : 10
    },
    headerTextStyle : {
        fontSize : 20,
        color: '#27282A',
        marginLeft : 10,
        marginRight : 5,
        fontWeight: '500',
    },

    artistTextStyle : {
        fontSize : 15,
        color: '#BBBCBE',
        fontWeight: '300',
        marginLeft : 10,
        marginRight : 5,
        marginBottom: 5,
    },
    imageStyle : {
        height : 300,
        flex : 1,
        width : null,
    }
    

}
export default Album;