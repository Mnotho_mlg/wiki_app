import React, {Component} from 'react';
import {Text, TouchableOpacity} from 'react-native';

const Button = ({onPress,text}) => //passing through the on press event handler (didn't use props because of destrucuring)
{ //touchable opacity that reacts to touches by changing the opacity
    const {buttonStyles, textStyle} = styles;
    return (
        <TouchableOpacity onPress={onPress} style={buttonStyles}>
            <Text style={textStyle}>{text}</Text>
        </TouchableOpacity>
    );
}

const styles = {

    buttonStyles : {
        flex : 1,
        alignSelf: 'stretch',
        backgroundColor : '#fff',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#007aff',
        marginLeft : 5,
        marginRight : 5,
        
    },

    textStyle : {
        alignSelf: 'center',
        color : '#007aff',
        fontSize : 16,
        fontWeight: '600',
        paddingTop : 10,
        paddingBottom : 10
    }
}
export {Button} ;