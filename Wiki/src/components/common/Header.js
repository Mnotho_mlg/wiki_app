//file that houses our header component
//import Libs
import React from 'react';
import {Text, View} from 'react-native';

//make the component
const Header = (props) => //Parent component must provide headerTest prop
{
    const {textStyle, viewStyle} = styles; //destr...

    return (

        <View style={viewStyle}> 
            <Text style={textStyle} >{props.headerText}</Text> 
        </View>
    );
};

//we'll having the styling inside the file

const styles = {

    viewStyle: {
        backgroundColor : '#007aff',
        justifyContent : 'center', //vertical placement
        alignItems : 'center', //horizontal placement
        height : 60,
        paddingTop: 10,
        shadowColor: '#000',
        shadowOffset: { width : 0, height : 5},
        shadowOpacity : 0.5,
        elevation : 2,
        position : 'relative'

    }, //styling the view wrapper
     
    textStyle: {
        fontSize: 15,
       fontWeight: '200',
       color : 'white'
    }
};

//make the component available to other parts (for rendering in other parts of the app)
export {Header};